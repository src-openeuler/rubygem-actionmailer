%global gem_name actionmailer

Name:		rubygem-%{gem_name}
Epoch:		1
Version:	7.0.7
Release:	2
Summary:	Email composition and delivery framework (part of Rails)
License:	MIT
URL:		https://rubyonrails.org
Source0:	https://rubygems.org/gems/%{gem_name}-%{version}.gem
# ActionMailer gem doesn't ship with the test suite.
# You may check it out like so
# git clone http://github.com/rails/rails.git
# cd rails/actionmailer && git archive -v -o actionmailer-7.0.4-tests.txz v7.0.4 test/
Source1: 	actionmailer-%{version}-tests.txz
# The tools are needed for the test suite, are however unpackaged in gem file.
# You may get them like so
# git clone http://github.com/rails/rails.git --no-checkout
# cd rails && git archive -v -o rails-7.0.4-tools.txz v7.0.4 tools/
Source2:	rails-%{version}-tools.txz
Patch3000:	backport-CVE-2024-47889.patch
Patch3001:	backport-CVE-2024-47889-test.patch

BuildRequires:	ruby(release)
BuildRequires:	rubygems-devel
BuildRequires:	ruby >= 2.2.2
BuildRequires:	rubygem(actionpack) = %{version}
BuildRequires:	rubygem(activejob)  = %{version}
BuildRequires:	rubygem(mail) >= 2.5.4
BuildRequires:  rubygem(net-smtp)
BuildArch:	noarch

%description
Email on Rails. Compose, deliver, and test emails using the familiar
controller/view pattern. First-class support for multipart email and
attachments.

%package	doc
Summary:	Documentation for %{name}
Requires:	%{name} = %{epoch}:%{version}-%{release}
BuildArch:	noarch

%description	doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}%{?prerelease} -b1 -b2
%patch3000 -p2

pushd %{_builddir}
%patch3001 -p2
popd

%build
gem build ../%{gem_name}-%{version}%{?prerelease}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
ln -s %{_builddir}/tools ..
mv %{_builddir}/test .

# Bigdecimal does not get auto-required
# https://github.com/rails/rails/issues/44399
ruby -Ilib:test -rbigdecimal -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.rdoc

%changelog
* Thu Oct 17 2024 yaoxin <yao_xin001@hoperun.com> - 1:7.0.7-2
- Fix CVE-2024-47889

* Thu Aug 17 2023 xu_ping <707078654@qq.com> - 1:7.0.7-1
- Upgrade to version 7.0.7

* Thu Jan 19 2023 wangkai <wangkai385@h-partners.com> - 1:7.0.4-1
- Upgrade to version 7.0.4

* Wed May 04 2022 wangkerong <wangkerong@h-partners.com> - 6.1.4.1-1
- Upgrade to 6.1.4.1

* Mon Feb  8 2021 sunguoshuai <sunguoshuai@huawei.com> - 5.2.4.4-1
- Upgrade to 5.2.4.4

* Sat Aug 8 2020 chengzihan <chengzihan2@huawei.com> - 5.2.3-1
- Package init
